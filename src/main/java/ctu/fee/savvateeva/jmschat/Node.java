package ctu.fee.savvateeva.jmschat;

/*
 * Class representing a node in a ring.
 */
public class Node {
	
	private FactoryController factory;
    
	private String nextNode;

    private String id; //ID must follow provider-specific naming convention
    private final String address;
    private final int port;
    private final String username;
    protected boolean isParticipant = false;

    
    public Node(String username, String address, Integer port) throws NumberFormatException {
    	
    	this.id = "Dest"+port.toString();
        this.address = address;
        this.port = port;
        if (username != null) this.username = username;
        else this.username = this.id;
        
        Main.logger.info("Node " + this.username + " created.\n");
    }
    
    public String getId() {
    	return id;
    }

	public String getAddress() {
		return address;
	}

	public int getPort() {
		return port;
	}

	public String getUsername() {
		return username;
	}

	public String getNextNode() {
		return nextNode;
	}

	public void setNextNode(String nextNode) {
		this.nextNode = nextNode;
	}

	public FactoryController getFactory() {
		return factory;
	}

	public void setFactory(FactoryController factory) {
		this.factory = factory;
	}

}
