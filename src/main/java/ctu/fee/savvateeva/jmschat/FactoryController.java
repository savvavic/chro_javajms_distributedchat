package ctu.fee.savvateeva.jmschat;

import java.io.IOException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.Queue;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;

import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.QueueConnectionFactory;

/*
 * ConnectionFactory builder.
 */
public class FactoryController {
	
	private QueueConnectionFactory factory;
    private Connection connection;
    private Session session;
    private Queue myQueue;
    private Queue theirQueue;
    private MessageProducer producer;
    private MessageConsumer consumer;
    private MessageListener messageListener;
    private Message connectMessage;
    private Message electionMessage;
    private Message leaderMessage;
    
    private Thread listener;
    
    private Node node;
    private String leader;
    
    private final String[] nodeFactoryNames;
    private final ArrayList<Node> nodeFactory = new ArrayList<Node>();

    /* Set the main parameters.
	 */
	public FactoryController() throws JMSException, SecurityException, IOException {
    	
    	this.nodeFactoryNames = new String[] {"172.22.36.2:7676", "172.22.36.2:7684", "172.22.36.2:7685", "172.22.36.2:7686",
    			"172.22.36.2:7687", "172.22.36.2:7688"};
    	setOrder();
    	this.factory = new com.sun.messaging.QueueConnectionFactory();
    	this.factory.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
    	this.connection = ((com.sun.messaging.ConnectionFactory) this.factory).createQueueConnection("admin", "admin");
    	this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }
	
	/* Set the other parameters.
	 */
	public void setParameters() throws JMSException, SecurityException, IOException {
		
		this.myQueue = this.session.createQueue(this.node.getId());
        this.consumer = this.session.createConsumer(this.myQueue);
        this.messageListener = new MessageListener(this.node, this);
        this.listener = new Thread(messageListener);
        this.listener.start();
    	this.consumer.setMessageListener(this.messageListener);
	}
	
	/*
	 * Send the first message.
	 */
    public void connect() throws JMSException, InterruptedException {
    	this.connection.start();
    	this.connectMessage = createMessage(this.node.getId(), "connect", getDateTime());
    	sendMessage(connectMessage, node.getNextNode());
    }
    
    /*
     * Set basic properties for a message.
     * @param sender sender ID
     * @param type message type (connect/next/election/leader/text/quit)
     * @return message Message object
     */
    public Message createMessage(String sender, String type, String datetime) throws JMSException {
    	
    	Message message = session.createMessage();
    	message.setStringProperty("sender", sender);
    	message.setStringProperty("type", type);
		message.setStringProperty("datetime", datetime);
		
		return message;
	}
    
    /*
     * Send the message.
     * @param message Message object
     * @param dest destination ID
     */
    public void sendMessage(Message message, String dest) throws JMSException, InterruptedException {
    	
    	this.theirQueue = this.session.createQueue(dest);
    	this.producer = this.session.createProducer(theirQueue);
    	this.myQueue = this.session.createQueue(dest);
    	this.consumer = this.session.createConsumer(this.myQueue);
    	
    	//redirect MessageListener to the next Node so we can switch to its view
    	messageListener.setNode(findNode(dest));
    	this.consumer.setMessageListener(messageListener);
    	this.setNode(findNode(dest));
    	producer.send(message);
    	producer.close();
    }
    
    public String getLeader() {
    	
    	return leader;
    }
    
    public void setLeader(String leader) {
		this.leader = leader;
	}
    
    public void setNode(Node node) {
    	
    	this.node = node;
    	this.node.setFactory(this);
    }
    
    public Node getNode() {
    	
    	return node;
    }
    
    public QueueConnectionFactory getFactory() {
		return factory;
	}

	public void setFactory(QueueConnectionFactory factory) {
		this.factory = factory;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Queue getMyQueue() {
		return myQueue;
	}

	public void setMyQueue(Queue myQueue) {
		this.myQueue = myQueue;
	}

	public Queue getTheirQueue() {
		return theirQueue;
	}

	public void setTheirQueue(Queue theirQueue) {
		this.theirQueue = theirQueue;
	}

	public MessageProducer getProducer() {
		return producer;
	}

	public void setProducer(MessageProducer producer) {
		this.producer = producer;
	}

	public MessageConsumer getConsumer() {
		return consumer;
	}

	public void setConsumer(MessageConsumer consumer) {
		this.consumer = consumer;
	}

	public MessageListener getMessageListener() {
		return messageListener;
	}

	public void setMessageListener(MessageListener messageListener) {
		this.messageListener = messageListener;
	}

	public Message getConnectMessage() {
		return connectMessage;
	}

	public void setConnectMessage(Message connectMessage) {
		this.connectMessage = connectMessage;
	}

	public Message getElectionMessage() {
		return electionMessage;
	}

	public void setElectionMessage(Message electionMessage) {
		this.electionMessage = electionMessage;
	}

	public Message getLeaderMessage() {
		return leaderMessage;
	}

	public void setLeaderMessage(Message leaderMessage) {
		this.leaderMessage = leaderMessage;
	}

	public Thread getListener() {
		return listener;
	}

	public void setListener(Thread listener) {
		this.listener = listener;
	}
	
	public String getDateTime() {
		
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		return Clock.systemUTC().instant().atZone(ZoneId.of("Europe/Paris")).format(myFormatObj);
	}

	public String[] getNodeFactoryNames() {
		return nodeFactoryNames;
	}
	
	public ArrayList<Node> getNodeFactory() {
		return nodeFactory;
	}
	
	/* Initialize the Node Factory. Define nodes from its primary names and set nexts.
	 */
	public void setOrder() {
		
		for (int i = 0; i < nodeFactoryNames.length; i++) {
			
			if (i == 0) {
				
				String next = getNodeFactoryNames()[i];
				Node nextNode = new Node(null, next.split(":")[0], Integer.parseInt(next.split(":")[1]));

			    if ((nodeFactoryNames.length == 1)) {
			    	nextNode.setNextNode(nextNode.getId());
			    	nodeFactory.add(nextNode);
			    }
			    else {
			    	
			    	 String nextNext =  getNodeFactoryNames()[i+1];
			    	 Node nextNextNode = new Node(null, nextNext.split(":")[0], Integer.parseInt(nextNext.split(":")[1]));
			    	 nextNode.setNextNode(nextNextNode.getId());
			    	 nodeFactory.add(nextNode);
			    	 nodeFactory.add(nextNextNode);
			    }
		    }
			else {
				if (i == nodeFactoryNames.length-1) nodeFactory.get(i).setNextNode(nodeFactory.get(0).getId());
				else {
					
					String nextNext = getNodeFactoryNames()[i+1];
					Node nextNextNode = new Node(null, nextNext.split(":")[0], Integer.parseInt(nextNext.split(":")[1]));
					nodeFactory.get(i).setNextNode(nextNextNode.getId());
					nodeFactory.add(nextNextNode);
				}
			}
	    }
	}
	
	/* Find Node by its ID.
	 * @param id Node ID
	 * @return Node instance
	 */
	public Node findNode(String id) {
		
		for (Node node : nodeFactory) {
			
			if (node.getId().equals(id)) return node;
		}
		
		return null;
	}
}
