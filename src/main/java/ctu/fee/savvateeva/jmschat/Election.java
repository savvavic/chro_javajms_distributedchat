package ctu.fee.savvateeva.jmschat;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;

/*
 * Chang-Roberts leader election.
 */
public class Election {
	
	protected final FactoryController factory;
	private static Election election = null;
	private boolean isActive = false;
	
	private Election(FactoryController factory) { this.factory = factory; }
	
	public static Election getInstance() {
		
		if (election == null) throw new AssertionError("Election not initialized.");
		return election;
	}
	
	public synchronized static void init(FactoryController factory) throws SecurityException, IOException {
		
		if (election == null) election = new Election(factory);
    }
	
	public void start() throws JMSException, InterruptedException {
		
		this.setIsActive(true);
		Message electionMessage = factory.createMessage(factory.getNode().getId(), "election", factory.getDateTime());
		electionMessage.setStringProperty("body", this.factory.getNode().getId());
		System.out.println(factory.getNode().getId() + " starts an election.\n");
		Main.logger.info("Election started by " + factory.getNode().getId() + ".\n");
		factory.sendMessage(electionMessage, factory.getNode().getNextNode());
		Thread.sleep(300);
	}
	
	public void defineLeader() throws JMSException, InterruptedException {
		
		String nodeId = factory.getNode().getId();
        factory.setLeader(nodeId);
        System.out.println("Defined Leader: " + factory.getLeader() + "\n");
        isActive = false;
        Message leaderMessage = factory.createMessage(nodeId, "leader", factory.getDateTime());
        leaderMessage.setStringProperty("body", nodeId);
        factory.sendMessage(leaderMessage, factory.getNode().getNextNode());
    }

	public boolean getIsActive() {
		
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		
		this.isActive = isActive;
		if (isActive == false) Main.logger.info("Election ended by " + factory.getNode().getId() + ".\n");
	}
}
