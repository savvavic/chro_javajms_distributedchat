# ChRo_JavaJMS_DistributedChat

Distributed chat application using Java JMS and Open Message Queue 5.1.1.

Windows
- install Open Message Queue 5.1.1 from [here](https://javaee.github.io/openmq/Downloads.html)
- run a broker (PowerShell - ./imqbrokerd.exe)
- import as Maven project
- run without any configuration - the program will ask for user data
- node IDs/usernames are available in the logs
