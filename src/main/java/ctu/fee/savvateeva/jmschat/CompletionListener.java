package ctu.fee.savvateeva.jmschat;

import java.util.logging.Logger;

import javax.jms.JMSException;
import javax.jms.Message;

/*
 * Completion listener.
 */
public class CompletionListener implements javax.jms.CompletionListener {
	
	private static final Logger completionLogger = Logger.getLogger(MessageListener.class.getName());
	
	private FactoryController factory;
	
	public CompletionListener(FactoryController factory) {
		this.factory = factory;
	}

	@Override
	public void onCompletion(Message message) {
		
		try { 
			completionLogger.info("Message " + message.getStringProperty("type").toUpperCase() + " successfully sent to " +
		factory.getNode().getNextNode()+".\n");
			if (message.getStringProperty("type").equals("text")) {
				Thread.sleep(500);
				if (factory.getLeader() == factory.getNode().getId()) {
                    System.out.println(message.getStringProperty("datetime") + " | From: " + message
                    		.getStringProperty("sender") + " | To: " + factory.getNode().getId() +
                    		" | Message: " + message.getStringProperty("body"));
                }
				else factory.sendMessage(message, factory.getNode().getNextNode());
			}
			}
		catch (JMSException | InterruptedException ex) { ex.printStackTrace();}//.getMessage()+"\n"); }
	}

	@Override
	public void onException(Message message, Exception exception) {
		
		completionLogger.warning(exception.getMessage()+"\n");
	}
}
