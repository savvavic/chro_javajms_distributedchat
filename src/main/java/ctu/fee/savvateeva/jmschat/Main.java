package ctu.fee.savvateeva.jmschat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.jms.JMSException;

/*
 * Initialize nodes and build a Connection Factory.
 */
public class Main {
	
	public static final Logger logger = Logger.getLogger(Main.class.getSimpleName());
	protected static FileHandler fileHandler;
	private static BufferedReader input;
	protected static FactoryController factoryController;
	
	
	/* Define Node Factory and ask for login.
	 * @param args empty
	 */
	public static void main(String[] args) throws NumberFormatException, JMSException, IOException, InterruptedException {
		
		try {
			
			setupLogger(logger);
			factoryController = new FactoryController();
			System.out.println("Enter username/ID:");
			input = new BufferedReader(new InputStreamReader(System.in));
			String inputUsername = input.readLine();
			
			if (factoryController.findNode(inputUsername) == null) {
				
				logger.info("Wrong userdata: " + inputUsername + "\n");
				System.err.print("No such node.\n");
				exitApp();
				return;
			}
			else {
				
				factoryController.setNode(factoryController.findNode(inputUsername));
				factoryController.setParameters();
				System.out.println("Success. " + inputUsername + " connecting to its next named " + factoryController.getNode()
				.getNextNode() + "\n");
				factoryController.connect();
				logger.info("Factory built, nodes connected.\n");
				Thread.sleep(300);
			}
		}
		catch (NumberFormatException | JMSException | IOException | InterruptedException ex) {
			
			System.err.println(ex.getMessage() + "\n");
			logger.severe(ex.getMessage()+"\n");
		}
	}
	
	/* Create a log file and append it to the logger.
	 * @logger Logger instance
	 */
	public static void setupLogger(Logger logger) throws SecurityException, IOException {
		
		fileHandler = new FileHandler("DSV_App_log.log", true);
		SimpleFormatter formatter = new SimpleFormatter();  
		fileHandler.setFormatter(formatter); 
		logger.addHandler(fileHandler); 
	}
	
	public static void exitApp() throws JMSException, InterruptedException {
		
		Main.logger.info("Exiting the application.\n");
		Thread.sleep(300);
		factoryController.getSession().close();
		factoryController.getConnection().close();
		Main.logger.info("=================END OF SESSION=================\n");
		Thread.sleep(300);
		fileHandler.close();
		System.out.println("Bye!");
		System.exit(0);
	}

}
