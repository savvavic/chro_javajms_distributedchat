package ctu.fee.savvateeva.jmschat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;

/*
 * Message listener.
 */
public class MessageListener extends Thread implements javax.jms.MessageListener {
	
	private FactoryController factory;
	private static Election election; // singleton
	private Node node; 

	private final BufferedReader input;
	
	protected ArrayList<Node> nodes;
	
	public MessageListener(Node node, FactoryController factory) throws JMSException, SecurityException, IOException {
		
		super();
		this.node = node;
		this.factory = factory;
		nodes = factory.getNodeFactory();
		this.input = new BufferedReader(new InputStreamReader(System.in));
		try {
			
			Election.init(factory); 
			election = Election.getInstance();
		}
		catch (AssertionError ex) { System.err.println(ex.getMessage()); }
		
		Main.logger.info("Message Listener for Node " + this.node.getUsername() + " set.\n");
	}
	
	@Override
	public void onMessage(Message message) {
		
		try {
			
			Main.logger.info("Message of type " + message.getStringProperty("type").toUpperCase() + " sent from " + message.getStringProperty("sender")
	        + " to " + this.node.getId() + "\n");
			Thread.sleep(300);

             switch(message.getStringProperty("type")) {	
			
				case "connect":
					if (node.getId().equals(message.getStringProperty("sender"))) {
						
						node.isParticipant = true;
						election.start();
					}
					else {
						
						Message next = factory.createMessage(this.node.getId(), "next", factory.getDateTime());
						factory.sendMessage(next, this.node.getNextNode());
					}
					break;
					
				case "next":
					election.start();
					break;
					
				case "election":
					
					try {
						
						Integer thisNode = Integer.parseInt(this.node.getId().substring(4));
						Integer senderNode = Integer.parseInt(message.getStringProperty("body").substring(4));
						
					    if (senderNode > thisNode) {
					    	
					    	Message nextMessage = factory.createMessage(this.node.getId(), message.
					    			getStringProperty("type"), factory.getDateTime());
					    	nextMessage.setStringProperty("body", message.getStringProperty("body"));
					    	System.out.println("Incoming node ID: " + senderNode + " is greater than" + " current Node ID: " + thisNode +
					    			" - forwarding message with the same ID " + senderNode + "\n");
					    	this.factory.getNode().isParticipant = true;
					    	election.setIsActive(true);
					        factory.sendMessage(nextMessage, this.node.getNextNode());
					    }
					    else if (senderNode < thisNode) {
					    	
					    	this.factory.getNode().isParticipant = true;
					    	election.setIsActive(true);
					    	Message nextMessage = factory.createMessage(this.node.getId(), message.
					    			getStringProperty("type"), factory.getDateTime());
					    	nextMessage.setStringProperty("body", node.getId());
					    	System.out.println("Incoming node ID: " + senderNode + " is smaller than " + "current Node ID: " + thisNode +
					    			" - forwarding message with current Node ID " + thisNode + "\n");
					        factory.sendMessage(nextMessage, this.node.getNextNode());
					    }
			            else {
			            	
			            	System.out.println("Current node ID:" + thisNode + " equals to " + "sender's node: " + senderNode + "\n");
					    	this.factory.getNode().isParticipant = false;
			            	election.defineLeader();
			            }
					}
					catch (NumberFormatException ex) {
						
						ex.printStackTrace();
						return;
					}
					break;
					
				case "leader":
					this.node.isParticipant = false;
					if (this.node.getId().equals(message.getStringProperty("sender"))) {
						
						Main.logger.info("LEADER: " + message.getStringProperty("sender") + ".\n");
						election.setIsActive(false);
						System.out.println("\nPrint:\n   ==exit for exit\n   ==logout for logging out\n   ==currentNode for checking current Node" +
								"'s ID\n   ==count for checking number of nodes.\n");
					}
					else factory.sendMessage(message, this.node.getNextNode());
			        break;
					 
				case "text":
					System.out.println("FROM: " + message.getStringProperty("sender") + " | " + message.getStringProperty("datetime") +
							"\n" + message.getStringProperty("body") + "\n");
					break;
					
				case "quit":
					if (nodes.size() != 0) System.out.println("Current Node: " + this.node.getUsername() + "\n");
					break;

				default:
					System.err.println("Unknown message type.");
					return;
			}
		}
		catch (JMSException | InterruptedException ex) {
			ex.printStackTrace();
		}
	}
	
	@Override
	public void run() {
	
		String line;
		
        while (true && (!Thread.currentThread().isInterrupted())) {
        	
        	if (Thread.currentThread().isInterrupted()) {
        		Main.fileHandler.close();
        		System.exit(0);
        		return;
        	}
        	
            try {
            	
                line = input.readLine();
                
                switch(line) {
                
                	case "==logout":
                		
                		nodes = factory.getNodeFactory();
                		
                		Node quittingNode = factory.findNode(this.node.getId());
                		if (nodes.size() == 1) quittingNode.setNextNode(this.node.getId());
                		int quittingIndex = nodes.indexOf(quittingNode);
                		Node prev = (quittingIndex == 0) ? (Node) nodes.get(nodes.size()-1) :
                				(Node) nodes.get(quittingIndex-1);
                		Node next = factory.findNode(quittingNode.getNextNode());
                		prev.setNextNode(next.getId());
                        Message quitMessage = factory.createMessage(quittingNode.getId(), "quit", factory.getDateTime());
                		factory.sendMessage(quitMessage, next.getId());
                		Main.logger.info("Node " + quittingNode.getId() + " is leaving the chat.\nCurrent Node: " + this.node.getId()+"\n");
                		Thread.sleep(300);
                		quittingNode.setNextNode(null);
                		factory.getNodeFactory().remove(quittingNode);
                		if (nodes.size() == 0) {
                			Main.logger.info("Chat empty.\n");
                    		Thread.sleep(300);
                			System.out.println("Chat empty!\n");
                			Main.exitApp();			
                		}
                        break;
                        
                	case "==exit":
                		 Main.exitApp();
                		 return;
                		 
                	case "==currentNode":
                		System.out.println("Current Node ID: " + this.node.getId()+"\n");
                		break;
                		
                	case "==count":
                		System.out.println("Number of participants: " + factory.getNodeFactory().size()+"\n");
                		break;

            		default:
            			Message textMessage = factory.createMessage(this.node.getId(), "text", factory.getDateTime());
            			textMessage.setStringProperty("body", line);
            			factory.sendMessage(textMessage, this.node.getNextNode());
            			break;
                }
            }
            catch (IOException | JMSException | InterruptedException ex) {
            	System.err.println(ex.getMessage());
            	Main.logger.warning(ex.getMessage());
            }
        }
	}
	
	public Node getNode() {
		return node;
	}

	public void setNode(Node node) throws InterruptedException {
		
		this.node = node;
		Main.logger.info("Message Listener for Node " + node.getUsername() + " set.\n");
		Thread.sleep(300);
	}

}
